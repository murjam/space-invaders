package ee.itcollege.space;

import ee.itcollege.space.field.GameField;
import ee.itcollege.space.lib.Direction;
import ee.itcollege.space.lib.Size;
import ee.itcollege.space.parts.Bullet;
import ee.itcollege.space.parts.SpaceShip;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * The main class for starting the "Space invaders" game
 * 
 * @author Mikk Mangus
 */
public class StartSpaceInvaders implements Runnable {

	private GameField game;
	private JFrame window = new JFrame("Space invaders");

	/**
	 * The main animator of the game
	 */
	private ActionListener animate = e -> {
		game.moveAndCheck();
		game.repaint();
	};

	private KeyListener control = new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			SpaceShip ship = game.getSpaceShip();
			int code = e.getKeyCode();

			switch (code) {
			case KeyEvent.VK_RIGHT:
				ship.moveIfPossible(Direction.RIGHT, game.getWall());
				break;
			case KeyEvent.VK_LEFT:
				ship.moveIfPossible(Direction.LEFT, game.getWall());
				break;
			case KeyEvent.VK_CONTROL:
			case KeyEvent.VK_SPACE:

				Bullet b = ship.fireBullet();
				if (b != null) {
					game.addBullet(b);
				}

				break;
			case KeyEvent.VK_ESCAPE                                                                                                                                                                      :
				System.exit(0);
				break;
			}
			game.repaint();
		}
	};
	
	/**
	 * Tries to set window full screen
	 * @return true, if success, false if failed
	 */
	private boolean setFullScreen() {
		GraphicsDevice[] devices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
		for (GraphicsDevice device : devices) {
			if (device.isFullScreenSupported()) {
				device.setFullScreenWindow(window);
				return true;
			}
		}
		return false;
	}

	@Override
	public void run() {

		boolean success = setFullScreen();
		if (success) {
			Size.setDimensions(window.getSize());
		}
		window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		window.setContentPane(game = new GameField());


		window.setVisible(true);
		Timer timer = new Timer(30, animate);
		timer.start();

		window.addKeyListener(control);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new StartSpaceInvaders());
	}
}
