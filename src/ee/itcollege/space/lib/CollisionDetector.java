package ee.itcollege.space.lib;

import java.awt.geom.Area;

/**
 * Checking the presence of collisions of {@link ICollidable} objects
 * 
 * @author Mikk Mangus
 */
public class CollisionDetector {

	/**
	 * Check if there is a collision between two {@link ICollidable} bojects
	 * @param o1
	 * @param o2
	 * @return true, if there is a collision
	 */
	public static boolean collide(ICollidable o1, ICollidable o2) {
		Area thisArea = o1.getArea();
		Area substracted = new Area(thisArea);

		substracted.subtract(o2.getArea());
		
		// if the substraction of areas changes the area, there is a collision
		return !thisArea.equals(substracted);
	}

}
