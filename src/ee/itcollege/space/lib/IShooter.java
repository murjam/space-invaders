package ee.itcollege.space.lib;

import ee.itcollege.space.parts.Bullet;

/**
 * Every object that can fire a bullet
 * 
 * @author Mikk Mangus
 */
public interface IShooter {

	public Bullet fireBullet();

}
