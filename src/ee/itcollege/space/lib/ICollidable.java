package ee.itcollege.space.lib;

import java.awt.geom.Area;

/**
 * An interface for any collidable object
 * @author Mikk Mangus
 */
public interface ICollidable {
	/**
	 * Area for collisions
	 */
	public Area getArea();
}
