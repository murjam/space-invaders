package ee.itcollege.space.lib;

public enum Direction {
	LEFT, RIGHT, UP, DOWN;

	public boolean isOpposite(Direction other) {

		switch (other) {
		case DOWN:
			return this.equals(UP);
		case LEFT:
			return this.equals(RIGHT);
		case RIGHT:
			return this.equals(LEFT);
		case UP:
			return this.equals(DOWN);
		}

		throw new IllegalStateException("no such direction implemented: "
				+ other);
	}
}
