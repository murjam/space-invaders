package ee.itcollege.space.lib;

import java.awt.Graphics2D;

/**
 * Interface for a drawable object
 *
 * @author Mikk Mangus
 */
public interface IDrawable {
	/**
	 * The drawing function
	 * @param g - Graphics object to draw the object onto
	 */
	public void drawItself(Graphics2D g);

}