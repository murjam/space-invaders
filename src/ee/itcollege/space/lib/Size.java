package ee.itcollege.space.lib;

import java.awt.Dimension;

/**
 * A class with static methods and variables for dimensions calculations.
 * It is not best to use static methods for this, but the code does its job.
 * 
 * @author Mikk Mangus
 */
public class Size {
	
	// default values also matter when fullscreen did not succeed
	private static int pixelSize = 5;
	private static int windowWidth = 555;
	private static int windowHeight = 550;
	
	
	public static int getPixelSize() {
		return pixelSize;
	}
	
	public static int getWindowWidth() {
		return windowWidth;
	}
	
	public static int getWindowHeight() {
		return windowHeight;
	}

	public static void setDimensions(Dimension size) {
		windowWidth = (int) size.getWidth();
		windowHeight = (int) size.getHeight();
		pixelSize = windowWidth / 120;
	}
	
}
