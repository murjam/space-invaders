package ee.itcollege.space.lib;

import ee.itcollege.space.parts.Pixel;
import ee.itcollege.space.parts.PixelGroup;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Explodes any {@link PixelGroup}
 * 
 * @author Mikk Mangus
 */
public class Explosion extends PixelGroup {

	/** The speed vector for every pixel */
	private List<Point2D.Double> pixelSpeeds = new ArrayList<>();

	public Explosion(PixelGroup group) {
		this.blocks = group.getBlocks();
		ArrayList<Pixel> additionalBlocks = new ArrayList<>();
		for (Pixel block: this.blocks) {
			additionalBlocks.add(block.clone());
		}
		blocks.addAll(additionalBlocks);
		this.position = group.getPosition();

		for (int i = 0; i < blocks.size(); i++) {
			// every pixel is given a random speed vector
			pixelSpeeds.add(new Point2D.Double(
					Math.random() * 8 - 4, 
					Math.random() * 8 - 4));
		}
	}

	/** 
	 * Animates the explosion
	 * @return false if there are no blocks left
	 */
	public boolean animateExplosion() {
		if (blocks.isEmpty()) {
			return false;
		}

		for (int i = 0; i < blocks.size(); i++) {
			Pixel pixel = blocks.get(i);

			// change pixel color
			pixel.color = darken(pixel.color);
			if (Color.BLACK.equals(pixel.color)) {
				// remove the pixel if it has turned black
				blocks.remove(i);
				pixelSpeeds.remove(i);
				i--;
				continue;
			}

			// move pixel
			Point2D.Double speed = pixelSpeeds.get(i);
			pixel.position.x += speed.x;
			pixel.position.y += speed.y;

		}
		return true;
	}
	/**
	 * Darkenes the given color by random amount
	 * 
	 * @param color the {@link Color} to darken
	 * @return new {@link Color} that is darkened
	 */
	private Color darken(Color color) {
		int amount = (int) (Math.random() * 50);
		return new Color(
				Math.max(0, color.getRed() - amount),
				Math.max(0,	color.getGreen() - amount),
				Math.max(0, color.getBlue() - amount));
	}
}
