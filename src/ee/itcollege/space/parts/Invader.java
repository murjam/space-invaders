package ee.itcollege.space.parts;

import ee.itcollege.space.lib.IShooter;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

/**
 * The bad guy in the game
 * 
 * @author Mikk Mangus
 */
public class Invader extends PixelGroup implements IShooter {

	/** The direction of movement of all invaders */
	public static boolean toRight = true;
	/** The offset of all invaders */
	public static int offset = 20;

	public static void moveAllInvaders() {
		if (toRight) {
			offset += 1;
		} else {
			offset -= 1;
		}
	}

	private boolean bulletLoaded = false;

	/**
	 * A bulletLoader that loads the bullet at random interval
	 */
	private Timer bulletLoader = new Timer((int) (Math.random() * 1000) + 2000,
			e -> {
				if (Math.random() > .85) {
					bulletLoaded = true;
				}
			});

	public Invader(Point2D.Double offset) {
		super(new int[][] {
				{ 0, 1, 0, 0, 0, 1, 0 },
				{ 0, 1, 1, 1, 1, 1, 0 },
				{ 1, 1, 2, 1, 2, 1, 1 },
				{ 1, 1, 1, 1, 1, 1, 1 },
				{ 1, 0, 1, 0, 1, 0, 1 },
		}, new Color[] { new Color(255, 192, 203),
				Color.blue });

		position = offset;
		bulletLoader.start();
	}

	@Override
	public Bullet fireBullet() {
		if (bulletLoaded) {
			bulletLoaded = false;
			Bullet b = new InvaderBullet();
			b.fire(new Point2D.Double(getPosition().x + 3, getPosition().y + 6), this);
			return b;
		}

		return null;
	}

	@Override
	public Double getPosition() {
		Point2D.Double invaderPosition = super.getPosition();
		// the position of an invader is affected by the static offset
		return new Point2D.Double(invaderPosition.x + offset, invaderPosition.y);
	}

}
