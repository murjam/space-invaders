package ee.itcollege.space.parts;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;

import ee.itcollege.space.lib.ICollidable;
import ee.itcollege.space.lib.IDrawable;
import ee.itcollege.space.lib.Size;

/**
 * The wall/frame of the game
 * 
 * @author Mikk Mangus
 */
public class Wall implements IDrawable, ICollidable {

	/** Cache the area, no need to recalculate it */
	private Area area = new Area();

	public Wall() {
		int w = Size.getWindowWidth();
		int h = Size.getWindowHeight();
		int px = Size.getPixelSize();
		
		// left
		area.add(new Area(new Rectangle2D.Double(0, 0, px * 2, h)));
		// right
		area.add(new Area(new Rectangle2D.Double(w - px * 2, 0, px * 2, h)));

		// top
		area.add(new Area(new Rectangle2D.Double(0, 0, w, px * 2)));
		// bottom
		area.add(new Area(new Rectangle2D.Double(0, h - px * 2, w, px * 2)));
	}

	@Override
	public Area getArea() {
		return area;
	}

	@Override
	public void drawItself(Graphics2D g) {
		g.setColor(Color.gray);
		g.fill(getArea());
	}

}
