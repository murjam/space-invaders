package ee.itcollege.space.parts;

import ee.itcollege.space.lib.Size;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.geom.Point2D;

/**
 * A simple building block with static position, dynamic position and a color.
 */
public class Pixel {


	public Point2D.Double position;
	public Color color;

	public Pixel(int x, int y) {
		this(x, y, Color.white);
	}

	public Pixel(int x, int y, Color color) {
		position = new Point2D.Double(x, y);
		this.color = color;
	}

	public void drawItself(Point2D.Double offset, Graphics2D g) {
		g.setColor(color);
		g.fill(getArea(offset));
	}

	public Area getArea(Point2D.Double offset) {
		int size = Size.getPixelSize();
		return new Area(new Rectangle(
				(int) (offset.x * size + position.x * size),
				(int) (offset.y * size + position.y * size),
				size,
				size));
	}

	public Pixel clone() {
		return new Pixel((int)position.x, (int)position.y, color);
	}

}
