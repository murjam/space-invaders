package ee.itcollege.space.parts;

import java.awt.Color;
import java.awt.geom.Point2D;

public class InvaderBullet extends Bullet {

	public InvaderBullet() {
		super(new int[][] { { 1, 1 }, { 1, 1 } }, new Color[] { new Color(150,
				50, 50) });
		speed = new Point2D.Double(Math.random() - .5, 1.5);
	}
}
