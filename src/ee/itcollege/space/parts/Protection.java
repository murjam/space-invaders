package ee.itcollege.space.parts;

import java.awt.Color;
import java.awt.geom.Point2D;

public class Protection extends PixelGroup {

	public Protection(int x, int y) {
		super(new int[][] { { 1, 1 }, { 1, 1 }, }, new Color[] { Color.gray, });
		this.position = new Point2D.Double(x, y);
	}

}
