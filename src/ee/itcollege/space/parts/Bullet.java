package ee.itcollege.space.parts;

import java.awt.Color;
import java.awt.geom.Point2D;

import ee.itcollege.space.lib.IShooter;

public class Bullet extends PixelGroup {

	IShooter shooter;

	Point2D.Double speed = new Point2D.Double(0, -2);

	public Bullet() {
		super(new int[][] { { 1 }, { 1 }, }, new Color[] { Color.green, });
	}

	protected Bullet(int[][] structure, Color[] colors) {
		super(structure, colors);
	}

	public void fire(Point2D.Double location, IShooter shooter) {
		this.position = location;
		this.shooter = shooter;
	}

	public void move() {
		position.x += speed.x;
		position.y += speed.y;
	}

	public IShooter getShooter() {
		return shooter;
	}
}
