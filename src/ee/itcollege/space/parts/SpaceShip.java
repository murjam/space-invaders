package ee.itcollege.space.parts;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;

import javax.swing.Timer;

import ee.itcollege.space.lib.CollisionDetector;
import ee.itcollege.space.lib.Direction;
import ee.itcollege.space.lib.ICollidable;
import ee.itcollege.space.lib.IShooter;
import ee.itcollege.space.lib.Size;

/**
 * The user-controllable spaceship
 * 
 * @author Mikk Mangus
 */
public class SpaceShip extends PixelGroup implements IShooter {

	/** The spaceship can only shoot when there is a bullet loaded */
	private boolean bulletLoaded = true;

	Timer bulletLoader = new Timer(500, new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			bulletLoaded = true;
		}
	});

	public SpaceShip() {
		super(new int[][] { { 0, 0, 0, 1, 0, 0, 0 }, { 0, 0, 0, 1, 0, 0, 0 },
				{ 0, 0, 1, 1, 1, 0, 0 }, { 1, 0, 1, 1, 1, 0, 1 },
				{ 1, 1, 1, 1, 1, 1, 1 }, { 1, 0, 1, 2, 1, 0, 1 }, },
				new Color[] { Color.white, Color.orange });
		position.x = 56;
		position.y = Size.getWindowHeight() / Size.getPixelSize() - 10;
		bulletLoader.setRepeats(false);
	}

	/**
	 * Moves the spaceship to the direction, only if there is no collision
	 * to the object given.
	 * 
	 * @param direction The direction to move the spaceship, only left and right implemented
	 * @param object The {@link ICollidable} Object tat
	 */
	public void moveIfPossible(Direction direction, ICollidable object) {
		switch (direction) {
		case RIGHT:
			position.x += 2;
			break;
		case LEFT:
			position.x -= 2;
			break;

		default:
			throw new IllegalStateException("SpaceShip has not implemented moving to " + direction);
		}
		if (CollisionDetector.collide(this, object)) {
			if (Direction.RIGHT.equals(direction)) {
				position.x -= 2;
			} else {
				position.x += 2;
			}
		}
	}

	@Override
	public Bullet fireBullet() {
		if (bulletLoaded) {
			bulletLoaded = false;
			bulletLoader.start();
			Bullet b = new Bullet();
			b.fire(new Point2D.Double(getPosition().x + 3, getPosition().y),
					this);
			return b;
		}

		return null;
	}

}
