package ee.itcollege.space.field;

import ee.itcollege.space.StartSpaceInvaders;
import ee.itcollege.space.lib.CollisionDetector;
import ee.itcollege.space.lib.Explosion;
import ee.itcollege.space.lib.Size;
import ee.itcollege.space.parts.*;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * Applet for drawing gamefield
 */
@SuppressWarnings("serial")
public class GameField extends JPanel {

	/** The user-controlled spaceship */
	private SpaceShip spaceShip = new SpaceShip();
	/** The frame of the gamefield */
	private Wall wall;
	/** The bad guys */
	private List<Invader> invaders = new ArrayList<>();
	/** Currently flying bullets */
	private List<Bullet> bullets = new ArrayList<>();
	/** Currently happening explosions */
	private List<Explosion> explosions = new ArrayList<>();
	/** The shield for the spaceship */
	private List<Protection> shield = new ArrayList<>();

	private Image buffer;
	private int points = 0;
	private int lives = 2;
	private int level = 0;

	private int invaderMoveCount = 0;
	
	public GameField() {
		buffer = new BufferedImage(Size.getWindowWidth(), Size.getWindowHeight(), BufferedImage.TYPE_INT_ARGB);
		wall = new Wall();
		createProtection();
	}

	private void createProtection() {
		int px = Size.getPixelSize();
		int offsetX = 28;
		int offsetY = Size.getWindowHeight() / px - 20;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 6; j++) {
				for (int j2 = 0; j2 < 3; j2++) {
					Protection p = new Protection(offsetX + i * 20 + j * 2, offsetY + j2);
					addProtection(p);
				}
			}
		}
	}

	private Image getBuffer() {
		// fill the image with white
		Graphics g = buffer.getGraphics();
		g.setColor(Color.black);
		g.fillRect(0, 0, Size.getWindowWidth(), Size.getWindowHeight());

		return buffer;
	}
	

	@Override
	public void paintComponent(Graphics g) {
		Image buffer = getBuffer();
		Graphics2D g2 = (Graphics2D) buffer.getGraphics();

		for (Explosion explosion : explosions) {
			explosion.drawItself(g2);
		}

		for (Protection p : shield) {
			p.drawItself(g2);
		}

		wall.drawItself(g2);
		spaceShip.drawItself(g2);

		for (Invader invader : invaders) {
			invader.drawItself(g2);
		}

		for (Bullet bullet : bullets) {
			bullet.drawItself(g2);
		}
		int px = Size.getPixelSize();
		g2.setColor(Color.green);
		g2.setFont(new Font(Font.MONOSPACED, Font.BOLD, 20));
		g2.drawString(String.format("points: %3d", points), Size.getWindowWidth() - px * 3 - 140, Size.getWindowHeight() - px * 3);
		g2.setColor(Color.red);
		g2.drawString(String.format("lives: %d", lives), px * 3, Size.getWindowHeight() - px * 3);

		g.drawImage(buffer, 0, 0, null);
	}

	public SpaceShip getSpaceShip() {
		return spaceShip;
	}

	public Wall getWall() {
		return wall;
	}

	public List<Invader> getInvaders() {
		return invaders;
	}

	private void setInvadersByLevel() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < level; j++) {
				invaders.add(new Invader(new Point2D.Double(i * 10 + 3, j * 10 + 3)));
			}
		}
	}

	public void moveAndCheck() {

		if (invaders.isEmpty()) {
			level++;
			lives++;
			setInvadersByLevel();
			Invader.offset = 0;
		}

		// check for collisions between bullets and everything
		bulletLoop: for (int i = 0; i < bullets.size(); i++) {
			Bullet bullet = bullets.get(i);
			bullet.move();

			if (CollisionDetector.collide(bullet, wall)) {
				explosions.add(new Explosion(bullets.remove(i)));
				i--;
				continue;
			}
			
			if (CollisionDetector.collide(bullet, spaceShip)) {
				explosions.add(new Explosion(bullets.remove(i)));
				i--;

				if (spaceShip.getHealthPercent() > 90) {
					explosions.add(new Explosion(spaceShip.removeRandomPixels(4)));
				} else {
					lives--;
					explosions.add(new Explosion(spaceShip));
					if (lives < 0) {
						Timer t = new Timer(1000, e -> System.exit(0));
						t.setRepeats(false);
						t.start();
					} else {
						Timer t = new Timer(1000, e -> spaceShip = new SpaceShip());
						t.setRepeats(false);
						t.start();
					}
				}
				continue;
			}

			if (bullet.getShooter() == spaceShip) {
				// kontrollin, kas koll saab pihta
				for (int j = 0; j < invaders.size(); j++) {
					if (CollisionDetector.collide(bullet, invaders.get(j))) {
						// koll sai pihta
						points = points + 1;
						if (invaders.get(j).getHealthPercent() > 90) {
							explosions.add(new Explosion(invaders.get(j).removeRandomPixels(5)));
						} else {
							explosions.add(new Explosion(invaders.remove(j)));
						}
						explosions.add(new Explosion(bullets.remove(i)));
						i--;
						continue bulletLoop;
					}
				}
			}

			for (int j = 0; j < shield.size(); j++) {
				if (CollisionDetector.collide(bullet, shield.get(j))) {
					explosions.add(new Explosion(bullets.remove(i)));
					explosions.add(new Explosion(shield.remove(j)));
					i--;
					continue bulletLoop;
				}
			}

			for (int j = 0; j < bullets.size(); j++) {
				if (i != j && CollisionDetector.collide(bullet, bullets.get(j))) {
					explosions.add(new Explosion(bullets.remove(i)));
					if (i < j) {
						j--;
					}
					explosions.add(new Explosion(bullets.remove(j)));
					i--;
					continue bulletLoop;
				}
			}
		}

		for (Invader invader : invaders) {

			Bullet b = invader.fireBullet();
			if (b != null) {
				bullets.add(b);
			}
		}

		// invaders move
		if (++invaderMoveCount > 10) {
			invaderMoveCount = 0;
			Invader.moveAllInvaders();
			for (Invader invader : invaders) {
				if (CollisionDetector.collide(wall, invader)) {
					Invader.toRight = !Invader.toRight;
					Invader.moveAllInvaders();
					break;
				}
			}
		}

		for (int i = 0; i < explosions.size(); i++) {
			if (!explosions.get(i).animateExplosion()) {
				explosions.remove(i);
				i--;
			}
		}

	}

	public void addBullet(Bullet b) {
		bullets.add(b);
	}

	private void addProtection(Protection p) {
		shield.add(p);
	}

	public static void main(String[] args) {
		// just delegating the start to SpaceStart
		StartSpaceInvaders.main(args);
	}

}
