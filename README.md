# Space Invaders game #

This is a Space Invaders game written in Java originally created in just one day as a part of a study program.

This is what the game looks like:
![space-invaders.png](https://bitbucket.org/repo/XL7Gy7/images/2644441477-space-invaders.png)

This is the original image this game took the styling from:
![space-invaders-orig.jpg](https://bitbucket.org/repo/XL7Gy7/images/1335290662-space-invaders-orig.jpg)